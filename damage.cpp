#include "damage.h"

Damage::Damage(int total, TypeDamage type)
    : m_totalDamage(total), m_typeDamage(type)
{

}

int Damage::totalDamage() const
{
    return m_totalDamage;
}

void Damage::setTotalDamage(int totalDamage)
{
    m_totalDamage = totalDamage;
}

void Damage::setTypeDamage(const TypeDamage &typeDamage)
{
    m_typeDamage = typeDamage;
}
