#include <QCoreApplication>
#include <QDebug>

#include "hero.h"
#include "event.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    srand(time(0));


    HeroFactory heroFactory;
    heroFactory.AutoRegister();

    QList<Hero *> heroes;
    Hero* gunzzy = heroFactory.Create("warrior", "Gunzzy");
    Hero* curseur = heroFactory.Create("mage", "Curseur");
    Hero* spaaceMonkey = heroFactory.Create("rogue", "SpaaceMonkey");


    EventFactory eventFactory;
    eventFactory.AutoRegister();

    QList<Event *> events;
    Event* swordSlash = eventFactory.Create("swordSlash");
    Event* fireSwordSlash = eventFactory.Create("fireSwordSlash");
    Event* fireBall = eventFactory.Create("fireBall");
    Event* poisonedTrap = eventFactory.Create("poisonedTrap");

    heroes.append(gunzzy);
    heroes.append(curseur);
    heroes.append(spaaceMonkey);

    events.append(swordSlash);
    events.append(fireSwordSlash);
    events.append(fireBall);
    events.append(poisonedTrap);



    while (true) {
        qDebug() << "\n" << "Tous les Heros commencent avec 20 points de vie.";

        Event* event = events.at(rand()%events.size());
        qDebug() << "\n" << "Event : " << event->type() << ": physical damages =" <<  event->physicalDamage() << ","
                 << "magic damages =" << event->magicDamage() << ","
                 << "trap damages =" << event->trapDamage() << ".";
        QList<Hero *> survives;
        for(auto hero: heroes) {
            if(!hero->DamageTaken(event)){
                survives.append(hero);
                continue;
            }
            qDebug() << hero->name() << "est mort !";
        }

        if(survives.length() == 1){
            qDebug() << "\n" << survives.at(0)->name() << "est le vrai HERO !";
            break;
        }

        if(survives.length() == 0){
            qDebug() << "\n" << "Tout le monde est mort, pas de chance";
            break;
        }
        qDebug() << "\n" << "Round suivant" << "\n";
    };

    qDebug() << "\n" << "Fin du jeu";

    return a.exec();
}
