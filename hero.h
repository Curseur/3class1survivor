#ifndef HERO_H
#define HERO_H

#include "damage.h"
#include "event.h"

#include <QMap>
#include <QString>
#include <QVector>

class Hero
{
private:
    QString m_classe;
    QString m_name;
    int m_Maxhealth;
    int m_health;

public:
    virtual bool DamageTaken(Event* event) = 0;

    QString classe() const;
    void setClasse(const QString &classe);

    QString name() const;
    void setName(const QString &name);

    int health() const;
    void setHealth(int health);
};

template <typename T>
class THero : public Hero {
public:
    static Hero *create();
};

class Warrior : public THero<Warrior>
{
private:

public:
    bool DamageTaken(Event* event);
};

class Mage : public THero<Mage>
{
private:

public:
    bool DamageTaken(Event* event);
};

class Rogue : public THero<Rogue>
{
private:

public:
    bool DamageTaken(Event* event);
};


typedef Hero *(*CreateHeroFn)();

class HeroFactory {
private:
    QVector<Hero *> heroes;
    QMap<QString, CreateHeroFn> registeredFunctions;

public:
    HeroFactory();
    ~HeroFactory();

    void Register(QString classe, CreateHeroFn fn);
    void AutoRegister();
    void KillThemAll();
    Hero* Create(QString classe, QString name);
};

#endif // HERO_H
