#ifndef EVENT_H
#define EVENT_H

#include <QMap>
#include <QVector>

class Event
{
private:
    int m_physicalDamage;
    int m_magicDamage;
    int m_trapDamage;
    QString m_type;

public:
    int physicalDamage() const;
    void setPhysicalDamage(int physicalDamage);

    int magicDamage() const;
    void setMagicDamage(int magicDamage);

    int trapDamage() const;
    void setTrapDamage(int trapDamage);

    QString type() const;
    void setType(const QString &type);
};

template <typename F>
class TEvent : public Event {
public:
    static Event *create(int physicalDamage = 0, int magicDamage = 0, int trapDamage = 0);
};

class SwordSlash : public TEvent<SwordSlash> {
private:

public:

};

class FireSwordSlash : public TEvent<FireSwordSlash> {
private:

public:

};

class FireBall : public TEvent<FireBall> {
private:

public:

};

class PoisonedTrap : public TEvent<PoisonedTrap> {
private:

public:

};


typedef Event *(*CreateEventFn)(int, int, int);

class EventFactory {
private:
    QVector<Event *> events;
    QMap<QString, CreateEventFn> registeredFunctions;
    QMap<QString, QVector<int>> registeredValues;

public:
    EventFactory();

    void Register(QString type, CreateEventFn fn, int physicalDmg, int magicDmg, int trapDmg);
    void AutoRegister();
    Event* Create(QString type);
};

#endif // EVENT_H
