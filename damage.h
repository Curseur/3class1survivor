#ifndef DAMAGE_H
#define DAMAGE_H

enum class TypeDamage {
    PHYSICAL, MAGIC, TRAP
};

class Damage
{
private:
    int m_totalDamage;
    TypeDamage m_typeDamage;

public:
    Damage(int total, TypeDamage type);

    int totalDamage() const;
    void setTotalDamage(int totalDamage);

    TypeDamage typeDamage() const;
    void setTypeDamage(const TypeDamage &typeDamage);
};

#endif // DAMAGE_H
