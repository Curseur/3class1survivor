#include "hero.h"

#include <QtDebug>

template<typename T>
Hero *THero<T>::create()
{
    qDebug() << __PRETTY_FUNCTION__;
    T* newClass = new T;
    newClass->setHealth(20);
    return newClass;
}

QString Hero::classe() const
{
    return m_classe;
}

void Hero::setClasse(const QString &classe)
{
    m_classe = classe;
}

QString Hero::name() const
{
    return m_name;
}

void Hero::setName(const QString &name)
{
    m_name = name;
}

int Hero::health() const
{
    return m_health;
}

void Hero::setHealth(int health)
{
    m_health = health;
}


bool Warrior::DamageTaken(Event *event)
{
    if(health() <= 0) return true;
    int finalPhysicalDmg = event->physicalDamage() - 3 >= 0 ? event->physicalDamage() - 3 : 0;
    int finalMagicDmg = event->magicDamage() * 2;
    int finalTrapDmg = event->trapDamage() + 1;

    setHealth(health() - finalPhysicalDmg - finalMagicDmg - finalTrapDmg);
    qDebug() << name() << classe() << ": " << health() << "Points de vie restant " << "(-"
             << finalPhysicalDmg << "Physic.Dmg, -"
             << finalMagicDmg << "Magic.Dmg, -"
             << finalTrapDmg << "Trap.Dmg)";
    return health() <= 0;
}

bool Mage::DamageTaken(Event *event)
{
    if(health() <= 0) return true;
    int finalPhysicalDmg = event->physicalDamage() + 1;
    int finalMagicDmg = event->magicDamage() - 3 >= 0 ? event->magicDamage() - 3 : 0;
    int finalTrapDmg = event->trapDamage() * 2;

    setHealth(health() - finalPhysicalDmg - finalMagicDmg - finalTrapDmg);
    qDebug() << name() << classe() << ": " << health() << "Points de vie restant " << "(-"
             << finalPhysicalDmg << "Physic.Dmg, -"
             << finalMagicDmg << "Magic.Dmg, -"
             << finalTrapDmg << "Trap.Dmg)";
    return health() <= 0;
}

bool Rogue::DamageTaken(Event *event)
{
    if(health() <= 0) return true;
    int finalPhysicalDmg = event->physicalDamage() * 2;
    int finalMagicDmg = event->magicDamage() + 1;
    int finalTrapDmg = event->trapDamage() - 3 >= 0 ? event->trapDamage() - 3 : 0;

    setHealth(health() - finalPhysicalDmg - finalMagicDmg - finalTrapDmg);
    qDebug() << name() << classe() << ": " << health() << "Points de vie restant " << "(-"
             << finalPhysicalDmg << "Physic.Dmg, -"
             << finalMagicDmg << "Magic.Dmg, -"
             << finalTrapDmg << "Trap.Dmg)";
    return health() <= 0;
}



HeroFactory::HeroFactory()
{

}

HeroFactory::~HeroFactory()
{
    KillThemAll();
}

void HeroFactory::Register(QString classe, CreateHeroFn fn)
{
    qDebug() << __PRETTY_FUNCTION__ << classe;
    registeredFunctions[classe] = fn;
}

void HeroFactory::AutoRegister()
{
    Register("warrior", &Warrior::create);
    Register("mage", &Mage::create);
    Register("rogue", &Rogue::create);
}

void HeroFactory::KillThemAll()
{
    qDebug() << __PRETTY_FUNCTION__;
    for(auto hero : qAsConst(heroes)) {
        delete hero;
    }
}

Hero *HeroFactory::Create(QString classe, QString name)
{
    qDebug() << __PRETTY_FUNCTION__ << classe << name;
    Hero *h;
    CreateHeroFn fn = registeredFunctions[classe];
    if(fn != nullptr) {
        h = fn();
        h->setClasse(classe);
        h->setName(name);
        heroes.push_back(h);
        return h;
    }
    qDebug() << "Classe non supporté" << classe;
    return nullptr;
}
