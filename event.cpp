#include "event.h"

#include <QDebug>

template<typename F>
Event *TEvent<F>::create(int physicalDamage, int magicDamage, int trapDamage)
{
    qDebug() << __PRETTY_FUNCTION__;
    F* newClass = new F;

    newClass->setPhysicalDamage(physicalDamage);
    newClass->setMagicDamage(magicDamage);
    newClass->setTrapDamage(trapDamage);
    return newClass;
}

QString Event::type() const
{
    return m_type;
}

void Event::setType(const QString &type)
{
    m_type = type;
}

int Event::physicalDamage() const
{
    return m_physicalDamage;
}

void Event::setPhysicalDamage(int physicalDamage)
{
    m_physicalDamage = physicalDamage;
}

int Event::magicDamage() const
{
    return m_magicDamage;
}

void Event::setMagicDamage(int magicDamage)
{
    m_magicDamage = magicDamage;
}

int Event::trapDamage() const
{
    return m_trapDamage;
}

void Event::setTrapDamage(int trapDamage)
{
    m_trapDamage = trapDamage;
}


EventFactory::EventFactory()
{

}

void EventFactory::Register(QString type, CreateEventFn fn, int physicalDmg, int magicDmg, int trapDmg)
{
    qDebug() << __PRETTY_FUNCTION__ << type;
    registeredFunctions[type] = fn;
    QVector<int> toStore = QVector<int>();

    toStore.append(physicalDmg);
    toStore.append(magicDmg);
    toStore.append(trapDmg);

    registeredValues[type] = toStore;
}

void EventFactory::AutoRegister()
{
    Register("swordSlash", &SwordSlash::create, 4, 0, 0);
    Register("fireSwordSlash", &FireSwordSlash::create, 4, 4, 0);
    Register("fireBall", &FireBall::create, 2, 4, 0);
    Register("poisonedTrap", &PoisonedTrap::create, 1, 0, 4);
}

Event *EventFactory::Create(QString type)
{
    qDebug() << __PRETTY_FUNCTION__ << type;
    Event *newEvent;
    CreateEventFn fn = registeredFunctions[type];
    QVector<int> values = registeredValues[type];
    if(fn != nullptr) {
        newEvent = fn(values.at(0), values.at(1), values.at(2));
        newEvent->setType(type);
        events.push_back(newEvent);
        return newEvent;
    }
    qDebug() << "Type non supporté" << type;
    return nullptr;
}
